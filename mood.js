import { splitToLines } from "./utils.js";
import Database from "./db.js";

export default new Database(
  splitToLines("./mood.txt").map((entry) => {
    const [time, mood, note = ""] = entry.split("  ");

    if (isNaN(+mood)) console.log(entry)

    return { note, timestamp: new Date(time), mood: +mood };
  }),
);
