export const addDays = (date, n = 1) => new Date(+date + (864E5 * n));
export const duration = (start, end) => (+end - +start);
export const isValidDate = (d) => !Number.isNaN(d.valueOf());
export const printMonth = (d) => d.getFullYear() + "-" + pad(d.getMonth() + 1);

export const apostrophise = (s) => s.replace(/([A-Za-z])'/g, "$1’");
export const cap = (s) => s[0].toUpperCase() + s.slice(1).toLowerCase();
export const countWords = (s) => s.trim().split(/\s+/).length;
export const dewidow = (s) => s.replace(/ (?=[^ ]*$)/, "\u00A0");
export const pad = (n) => (n + "").padStart(2, "0");
export const squish = (s) => s.split("\n").reduce((x, i) => x + i.trim());
export const url = (s) =>
  s.toLowerCase().trim().replace(/ /g, "-").replace(/\W/g, "");

export const cagr = (past, now, n) => ((now / past) ** (1 / n) - 1) * 100;
export const clamp = (val, min, max) => Math.max(min, Math.min(max, val));
export const growth = (past, present) => (present - past) / past * 100;
export const normaliseRatio = (val, min, max) => (val - min) / (max - min);
export const trend = (x, y) => (x - y) / Math.abs(y);

export const shuffle = (arr) => arr.sort(() => 0.5 - Math.random());
export const randomItem = (arr) => arr[Math.floor(Math.random() * arr.length)];

export const box = (x) => ({ next: (f) => box(f(x)), done: (f) => f(x) });
export const pipe = (...fns) => (x) => fns.reduce((y, f) => f(y), x);
export const compose = (...fns) => (x) => fns.reduceRight((y, f) => f(y), x);
export const xor = (a, b) => (a && !b) || (!a && b);

export const avg = (set = []) => set.length === 0 ? 0 : sum(set) / set.length;
export const printDate = (date) => date.toISOString().split("T")[0];
export const toDate = (x) => x.toISOString().split("T")[0];

export function debounce(func, delay) {
  let timeoutId;

  return function(...args) {
    clearTimeout(timeoutId);

    timeoutId = setTimeout(() => {
      func.apply(this, args)
    }, delay)
  }
}

export function curry(fn, ...args) {
  return fn.length <= args.length ? fn(...args) : curry.bind(null, fn, ...args);
}

/**
 * omit keys from object
 * @param {object} obj
 * @param {array} keys
 * @returns object with omitted keys
 */
export function omit(obj, keys) {
  return Object.keys(obj)
    .filter((k) => !keys.includes(k))
    .reduce((res, k) => ({ ...res, [k]: obj[k] }), {});
}

/**
 * pick keys from object
 * @param {object} obj
 * @param {array} keys
 * @returns object with selected keys
 */
export function pick(obj, keys) {
  return Object.keys(obj)
    .filter((k) => keys.includes(k))
    .reduce((res, k) => ({ ...res, [k]: obj[k] }), {});
}

export function rgbToHex(r, g, b) {
  return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

export function splitToLines(file) {
  return Deno.readTextFileSync(file).trim().split("\n")
    .filter((line) => line.trim() !== "");
}

export function format(n) {
  return new Intl.NumberFormat("en", { maximumFractionDigits: 2 }).format(n);
}

export function toStamp(date) {
  const y = `${date.getFullYear()}`.slice(-2);
  const m = pad(date.getMonth() + 1);
  const d = pad(date.getDate());
  const H = pad(date.getHours());
  const M = pad(date.getMinutes());
  return y + m + d + H + M;
}

export function durationString(ms) {
  const seconds = ms / 1E3;
  const MINUTE_SECS = 60;
  const HOUR_SECS = MINUTE_SECS * 60;
  const DAY_SECS = HOUR_SECS * 24;
  const MONTH_SECS = DAY_SECS * 30;
  const YEAR_SECS = DAY_SECS * 365;

  const Y = Math.floor(seconds / YEAR_SECS);
  const M = Math.floor((seconds % YEAR_SECS) / MONTH_SECS);
  const D = Math.floor((seconds % MONTH_SECS) / DAY_SECS);
  const h = Math.floor((seconds % DAY_SECS) / HOUR_SECS);
  const m = Math.floor((seconds % HOUR_SECS) / 60);
  const s = Math.floor(seconds % 60);

  const combine = (obj) =>
    Object.entries(obj)
      .filter((a) => a[1] > 0)
      .reduce((acc, [sym, val]) => acc + val + sym, "");

  const period = combine({ Y, M, D });
  const time = combine({ h, m, s });

  // ISO 8601
  return `P${period}${time.length === 0 ? "" : 'T' + time.toUpperCase()}`;
}

export function isExternalUrl(str) {
  return str.startsWith("http://") || str.startsWith("https://");
}

export function ago(d) {
  const m = +d - new Date();
  const x = Math.abs(m);
  if (isNaN(x)) return "";

  const units = {
    year: 1e3 * 60 * 60 * 24 * 365.2425,
    month: 1e3 * 60 * 60 * 24 * (365.2425 / 12),
    week: 1e3 * 60 * 60 * 24 * 7,
    day: 1e3 * 60 * 60 * 24,
    hour: 1e3 * 60 * 60,
    minute: 1e3 * 60,
    second: 1e3,
  };

  const format = new Intl.RelativeTimeFormat("en", { numeric: "auto" });
  for (const u in units) {
    if (x > units[u] || u === "second") {
      return format.format(Math.round(m / units[u]), u);
    }
  }
}

export function listDates(start, end = new Date()) {
  const list = [];
  const now = new Date(start);

  while (now <= end) {
    list.push(printDate(now));
    now.setDate(now.getDate() + 1);
  }

  return list;
}

export function listMonths(start, end = new Date()) {
  const list = new Set();
  let now = new Date(start);
  end.setUTCHours(0, 0, 0, 0);

  while (now <= end) {
    list.add(now.toISOString().substring(7, 0));
    now = addDays(now, 1);
  }

  return [...list];
}

export function humanTime(n) {
  const p = (n, w) => `${format(Math.round(Math.abs(n)))}\u00A0${w}${+n.toFixed(0) !== 1 ? "s" : ""}`;
  const units = {
    year: 1e3 * 60 * 60 * 24 * 365.2425,
    month: 1e3 * 60 * 60 * 24 * (365.2425 / 12),
    week: 1e3 * 60 * 60 * 24 * 7,
    day: 1e3 * 60 * 60 * 24,
    hour: 1e3 * 60 * 60,
    minute: 1e3 * 60,
    second: 1e3,
  };

  const m = Math.abs(n);
  for (const u in units) {
    if (m > units[u] || u === "second") return p((n / units[u]), u);
  }
  return p(n, "second");
}

export function max(set = []) {
  let i = set.length;
  if (i === 0) return 0;
  let m = Number.MIN_VALUE;
  while (i--) {
    if (set[i] > m) m = set[i];
  }
  return m;
}

export function min(set = []) {
  let i = set.length;
  if (i === 0) return 0;
  let m = Number.MAX_VALUE;
  while (i--) {
    if (set[i] < m) m = set[i];
  }
  return m;
}

export function parseDate(str) {
  const [y, m, d, H = "00", M = "00", S = "00"] = str.match(/..?/g);
  return new Date(`20${y}-${m}-${d} ${H}:${M}:${S}`);
}

export function parseUTCDate(str) {
  const [y, m, d, H = "00", M = "00", S = "00"] = str.match(/..?/g);
  return new Date(`20${y}-${m}-${d} ${H}:${M}:${S}Z`);
}

export function printDateTime(date) {
  const h = pad(date.getHours());
  const m = pad(date.getMinutes());
  return `${printDate(date)} ${h}:${m}`;
}

export function range(set, format) {
  const n = min(set);
  const x = max(set);
  return {
    value: format(x - n),
    str: format(n) + "\u2013" + format(x),
  };
}

export function relativeUncertainty(
  set = [],
  cavg = undefined,
  csd = undefined,
) {
  if (set.length === 0) return 0;
  return (csd || standardError(set)) / (cavg || avg(set)) * 100;
}

export function sd(set = []) {
  const l = set.length;
  if (l === 0) return 0;
  const x = avg(set);
  let y = 0;
  for (let i = 0; i < l; i++) {
    y += (set[i] - x) ** 2;
  }
  return (y / (l - 1)) ** 0.5;
}

export function standardError(set = [], csd = undefined) {
  return set.length === 0 ? 0 : (csd || sd(set)) / Math.sqrt(set.length);
}

export function sum(set = []) {
  let i = set.length, n = 0;
  while (i--) n += set[i];
  return n;
}

export function summaryStats(set = []) {
  const l = set.length;
  let min = set[0] || 0;
  let max = min;
  let avg = 0;
  let sum = 0;

  for (let i = 0; i < l; i++) {
    const v = set[i];
    if (v > max) max = v;
    if (v < min) min = v;
    sum += v;
  }

  avg = sum / l;
  let y = 0;
  for (let i = 0; i < l; i++) {
    y += (set[i] - avg) ** 2;
  }

  return {
    sum,
    min,
    max,
    avg: sum / l,
    sd: (y / (l - 1)) ** 0.5,
    range: max - min,
  };
}

export function harmonicMean(set) {
  const sum = set.reduce((x, y) => x + (y ** -1), 0);
  return (sum / set.length) ** -1;
}
