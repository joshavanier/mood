import {
  addDays,
  duration,
  listDates,
  listMonths,
  printDate,
} from "./utils.js";

export default function Database(entries = []) {
  this.entries = entries;
  this.count = this.entries.length;
  this.cache = null;

  this.first = () => this.entries[0];
  this.last = () => this.entries.at(-1);

  this.averages = (key, sorted) => {
    sorted ||= this.sort();
    const _sum = (x, entry) => x + entry[key];
    return sorted.map((day) => {
      return day.length === 0 ? 0 : day.reduce(_sum, 0) / day.length;
    });
  };

  this.byDateCache = new Map();

  this.byDate = (date = new Date()) => {
    if (this.count === 0) return [];
    const ds = date.toDateString();
    if (this.byDateCache.has(ds)) {
      return this.byDateCache.get(ds);
    }
    const result = this.entries
      .filter(({ timestamp }) => timestamp.toDateString() === ds);

    this.byDateCache.set(ds, result);

    return result;
  };

  this.byPeriod = (start, end = new Date()) => {
    if (this.count === 0) return [];
    let subset = [];
    for (let now = start; now <= end;) {
      subset = [...subset, ...this.byDate(now)];
      now = addDays(now, 1);
    }
    return subset;
  };

  this.groupBy = (key) => {
    return this.entries.reduce((acc, item) => {
      return ((acc[item[key]] = [...(acc[item[key]] || []), item]), acc);
    }, {});
  };

  this.intervals = () => {
    if (this.count === 0) return [];
    const intervals = [];
    for (let i = 0; i < this.count - 1; i++) {
      intervals.push(
        duration(this.entries[i].timestamp, this.entries[i + 1].timestamp),
      );
    }
    return intervals;
  };

  this.list = (key) => this.entries.map((entry) => entry[key]);
  this.unique = (key) => [...new Set(this.list(key))];

  this.peakDays = (key) => {
    if (this.count === 0) return [];
    const days = Array(7).fill(0);
    for (const entry of this.entries) {
      days[entry.timestamp.getDay()] += entry[key];
    }
    return days;
  };

  this.peakMonths = (key) => {
    if (this.count === 0) return [];
    const months = Array(12).fill(0);
    for (const entry of this.entries) {
      months[entry.timestamp.getMonth()] += entry[key];
    }
    return months;
  };

  this.random = () => {
    return this.count
      ? this.entries[Math.floor(Math.random() * this.count)]
      : null;
  };

  this.recent = (days = 1) => this.sort().slice(-days).flat();

  this.sort = (end = new Date()) => {
    if (this.count === 0) return [];
    if (this.cache !== null) return this.cache;

    const sorted = listDates(this.first().timestamp, end)
      .reduce((a, v) => ({ ...a, [v]: [] }), {});

    for (const entry of this.entries) {
      sorted[printDate(entry.timestamp)]?.push(entry);
    }

    this.cache = Object.values(sorted);
    return this.cache;
  };

  this.sortByDay = () => {
    if (this.count === 0) return [];
    const week = [[], [], [], [], [], [], []];
    for (const entry of this.entries) {
      week[entry.timestamp.getDay()].push(entry);
    }
    return week;
  };

  this.sortByMonth = (end = new Date()) => {
    const month = (d) => d.toISOString().substring(7, 0);
    const months = listMonths(this.first().timestamp, end)
      .reduce((a, v) => ({ ...a, [v]: [] }), {});

    for (const entry of this.entries) {
      months[month(entry.timestamp)]?.push(entry);
    }

    return Object.values(months);
  };

  this.sums = (key, sorted) => {
    sorted ||= this.sort();
    const _sum = (x, entry) => x + entry[key];
    return sorted.map((day) => day.reduce(_sum, 0));
  };

  this.spawn = (entries) => new Database(entries);
}
