# little mood tracker

apologies for the mess. just wanted to upload these right away today

* mood.txt - a sample of my mood log. I use a shell script (see `/bin`) to add entries to this

* mood.js - parser for the mood.txt
* db.js - a database model for my Intra files, which are just varied text files but generally have the same set of attributes (timestamp, entry metadata)
* render.js - the thing that renders the static HTML mood tracker page
* utils.js - just a messy bunch of utility functions. it's shared across other Deno projects locally hence the chaos.

to run:
```
deno -WR render.js
```
