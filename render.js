import mood from "./mood.js";
import { avg, printMonth } from "./utils.js";

function render () {

  const _itemise = (acc, x) => `${acc}<p class="m${Math.round(x)}"></p>`

  const months = mood.sortByMonth();
  let html = "";

  for (const month of months) {
    const subset = mood.spawn(month)
    const sort = subset.sort(subset.last().timestamp)
    const averages = mood.averages("mood", sort)

    html += `<section><h2>${printMonth(subset.entries[0].timestamp)}</h2><div>${averages.reduce(_itemise, "")}</div></section>`
  }

  return `
    <!doctype html>
    <html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>mood</title>
    <link rel="icon" href="data:,">
    <link rel="stylesheet" href="mood.css">
    </head><body>
    <main>
    <article>
    ${html}
    </article>
    <footer>
    <div id="legend">
      <p><span class="m5"></span> v. happy</p>
      <p><span class="m4"></span> happy</p>
      <p><span class="m3"></span> neutral</p>
      <p><span class="m2"></span> sad</p>
      <p><span class="m1"></span> v. sad</p>
    </div>
    <p>mood log. ${mood.entries.length} entries. average mood is ${avg(mood.list("mood")).toFixed(2)} over 5.</p>
    </footer>
    </main>

    </body></html>`

}

Deno.writeTextFileSync('./output.html', render())