#!/usr/bin/env sh
# mood log

set -eu
S=$(basename -- "$0")
db=$HOME/intra/mood.txt

usage() {
  printf "? %s [command] [mood:1-5] [optional note]\n\
  l  log mood\n\
  e  edit log\n\
  u  undo last entry\n\
  v  view log\n\
  b  build\n" "$S"
  exit 1
}

[ "$#" -lt 1 ] && usage

log() {
  if [ "$#" -lt 2 ]; then
    printf "? %s %s [mood:1-5] [note]\n" "$S" "$1"
    exit 1
  fi
  case "$2" in
    *[!1-5]* )
      lu -e "invalid mood value; use 1-5"
      exit 1 ;;
  esac
  entry=$(printf "%s  %s  %s" "$(date -u +"%FT%T.%3NZ")" "$2" "${3:-""}")
  printf %s\\n "${entry%${entry##*[![:space:]]}}" | tee -a "$db"
}

undo() {
  lu -c "irreversible deletion of last entry"
  printf "proceed with deletion? (y/n): "
  read -r answer
  case $answer in
    [yY] )
      sed -i '$d' "$db"
      printf "last entry deleted\n" ;;
    * ) printf "aborted\n" ;;
  esac
}

case "$1" in
  l | log ) log "$@" ;;
  e | edit ) vim '+ normal GA' +startinsert "$db" ;;
  v | view ) tail "$db" ;;
  u | undo ) undo ;;
  * ) usage ;;
esac
