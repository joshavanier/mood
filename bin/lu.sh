#!/usr/bin/env sh
# script message util

set -eu

help () {
cat <<EOF
${0##*/}: script message utility
usage: ${0##*/} [opt] "message"

-c  print --caution
-e  print --error
-i  print --info
-s  print --success
-w  print --warning
EOF
exit 1
}

[ "$#" -lt 2 ] && help

rouge='\e[1;31m'
vert='\e[1;32m'
cyan='\e[1;36m'
jaune='\e[1;33m'

display () {
  echo "$1$2: $3\e[0m"
}

case "$1" in
  -c | --caution ) display "$jaune" caution "$2" ;;
  -e | --error ) display "$rouge" error "$2" ;;
  -i | --info ) display "$cyan" info "$2" ;;
  -s | --success ) display "$vert" success "$2" ;;
  -w | --warning ) display "$jaune" warning "$2" ;;
  * ) help ;;
esac
